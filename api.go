package main

import (
"net/http"
"io/ioutil"
"encoding/json"
"reflect"
"fmt"
	"log"
)

type balance struct {
	Address  string `json:"address"`
	AccountName            string `json:"accountname"`
	Balance          int64    `json:"balance"`
}
type holders struct {
	Address string `json:"address"`
	AssetID string `json:"assetId"`
	Balance int64  `json:"balance"`
}
func get_holders_by_addres() []balance{
	accounts:=[]string{"ITE Investment trust Security Account No 1", "ITE Investment trust Security Account No 2","ITE Investment trust Security Account No 3", "ITE Investment trust Security Account No 4", "J Scott-Thompson atf International Investment Trust", "J Scott-Thompson atf APIF Trust", "J Scott-Thompson atf International Trade Exchange Trust","J Scott-Thompson atf Reserve Account No 2", "J Scott-Thompson Reserve Account No 1"}
	info := map[string]string{
		"3P7Jh6tfwAJkCfzpo6dwvW7vvKgrbpMRfKm": accounts[0],
		"3PLND7xhE6X4MEMyx8HVPSResS4nZ8XiVq3": accounts[1],
		"3PBAoDi79itvw7MZaJzNpTSWZ4RU9CwdCFY": accounts[2],
		"3PDPw7eL3YGGzxCLvzC1xfc7o5b8BjLFfEG": accounts[3],
		"3PBztGMqYeyHz8f5dtoa6YbWMZbsA3z23VD": accounts[4],
		"3PBvH5kc1ET77e9J1tqvpBdTBKqN7sFQKVc": accounts[5],
		"3P51NYYoSqBjfJCoUH5bYncH5SL1j7Uunun": accounts[6],
		"3PMyFHxg8byNfzdNwNVBS3asrqHL6TVz5aM": accounts[7],
		"3PQjg9r5aTscnsVuGD4AqP3kXoGHqXLox3o": accounts[8],
	}

	keys := reflect.ValueOf(info).MapKeys()
	responce:=make([]balance,len(keys))
	strkeys := make([]string, len(keys))
	for i := 0; i < len(keys); i++ {
		strkeys[i] = keys[i].String()
	}

	for i:=0; i<len(keys); i++{
		url := "http://192.168.1.132:6869/assets/balance/"+strkeys[i]+"/4eooJSqSpHCzHp6Ao26maWBCR57gjWegbKbPDRhZ7YRt";
		res, err := http.Get(url)
		if err != nil {
			panic(err.Error())
		}
		body, err := ioutil.ReadAll(res.Body)
		if err != nil {
			panic(err.Error())
		}
		defer res.Body.Close()
		var data holders
		json.Unmarshal([]byte(body), &data)
		responce[i].Address = strkeys[i]
		responce[i].AccountName=info[keys[i].String()]
		responce[i].Balance=data.Balance
	}
	return responce;
}
func handler(w http.ResponseWriter, r *http.Request) {
	var resp []balance;
	resp=get_holders_by_addres()
	res, err := json.Marshal(resp)
	if err != nil {
		fmt.Println(err)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(res);
}

func main(){
	url:="192.168.1.115:8080";
	http.HandleFunc("/", handler) // set router
	err := http.ListenAndServe(url, nil) // set listen port

	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
